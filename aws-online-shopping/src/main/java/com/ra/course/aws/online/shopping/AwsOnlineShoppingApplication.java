package com.ra.course.aws.online.shopping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@SuppressWarnings("PMD")
public class AwsOnlineShoppingApplication {
    public static void main(String[] args) {
        SpringApplication.run(AwsOnlineShoppingApplication.class, args);
    }
}
