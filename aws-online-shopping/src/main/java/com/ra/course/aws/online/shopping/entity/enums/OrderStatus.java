package com.ra.course.aws.online.shopping.entity.enums;

public enum OrderStatus {
    UNSHIPPED, PENDING, SHIPPED, COMPLETE, CANCELED, REFUND_APPLIED;
}
