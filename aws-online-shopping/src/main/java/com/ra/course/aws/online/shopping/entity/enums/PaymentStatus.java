package com.ra.course.aws.online.shopping.entity.enums;

public enum PaymentStatus {
    UNPAID, PENDING, COMPLETED, FAILD, DECLINED, CANCELED, ABANDONED, SETTLING, SETTLED, REFUNDER
}
