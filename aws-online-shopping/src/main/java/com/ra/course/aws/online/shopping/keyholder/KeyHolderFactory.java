package com.ra.course.aws.online.shopping.keyholder;

import org.springframework.jdbc.support.KeyHolder;

public interface KeyHolderFactory {

    KeyHolder newKeyHolder();

}
