package com.ra.course.aws.online.shopping.entity.enums;

public enum ShipmentStatus {
    PENDING, SHIPPED, DELIVERED, ONHOLD
}
