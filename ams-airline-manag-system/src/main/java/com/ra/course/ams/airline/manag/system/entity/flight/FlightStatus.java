package com.ra.course.ams.airline.manag.system.entity.flight;

public enum FlightStatus {
    ACTIVE,
    SCHEDULED,
    DELAYED,
    DEPARTED,
    LANDED,
    INAIR,
    ARRIVED,
    CANCELED,
    DIVERTED,
    UNKNOWN
}
