package com.ra.course.ams.airline.manag.system.entity.flight;

public enum SeatClass {
    ECONOMY,
    ECONOMYPLUS,
    PREFERREDECONOMY,
    BUSINESS,
    FIRSTCLASS
}
