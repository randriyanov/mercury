package com.ra.course.ams.airline.manag.system.entity;

public enum ReservationStatus {
    REQUESTED,
    PENDING,
    CONFIRMED,
    CHECKEDIN,
    CANCELED,
    ABANDONED
}
