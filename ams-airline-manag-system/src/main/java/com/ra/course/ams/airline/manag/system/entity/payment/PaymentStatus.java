package com.ra.course.ams.airline.manag.system.entity.payment;

public enum PaymentStatus {
    UNPAID,
    PENDING,
    COMPLETED,
    FAILED,
    DECLINED,
    CANCELLED,
    ABANDONED,
    SETTLING,
    SETTLED,
    REFUNDED
}
