package com.ra.course.ams.airline.manag.system.entity.person;

public enum AccountStatus {
    ACTIVE,
    CLOSED,
    CANCELED,
    BLACKLISTED,
    BLOCKED
}
