package com.ra.course.ams.airline.manag.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@SuppressWarnings("PMD")
public class AirlineSpringApplication {
    public static void main(String[] args) {
        SpringApplication.run(AirlineSpringApplication.class, args);
    }
}
