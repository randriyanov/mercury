package com.ra.course.ams.airline.manag.system.entity.flight;

public enum SeatType {
    REGULAR,
    ACCESSIBLE,
    EMERGENCYEXIT,
    EXTRALEGROOM
}
